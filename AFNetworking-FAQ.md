## I'm Getting Compiler Errors About `dispatch_queue_t` Being a `strong` `@property`, What's Up With That?

As of iOS 6, `dispatch_queue_t` and other dispatch types are treated as objects, and as such, the correct `@property` ownership is `strong`. If you're getting a warning about this from Xcode, check that your targeted SDK is at least iOS 6 or Mac OS X 10.8, the minimum development targets for AFNetworking.

## Does AFNetworking have any caching mechanisms built-in?

AFNetworking takes advantage of the caching functionality already provided by `NSURLCache` and any of its subclasses. So long as your `NSURLRequest` objects have the correct cache policy, and your server response contains a valid `Cache-Control` header, responses will be automatically cached for subsequent requests.

## Can I use AFNetworking for OAuth?

Yes. Check out [AFOAuth1Client](https://github.com/AFNetworking/AFOAuth1Client) or [AFOAuth2Client](https://github.com/AFNetworking/AFOAuth2Client).

## What's the best way to test/mock network requests with AFNetworking?

Use [NSURLProtocol](https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSURLProtocol_Class/Reference/Reference.html) to intercept requests before they hit the network, and return canned responses using local data. More information about this technique [can be found in this blog post by Claus Broch](http://www.infinite-loop.dk/blog/2011/09/using-nsurlprotocol-for-injecting-test-data/). 

There are also some great third-party libraries, including [Nocilla](https://github.com/luisobo/Nocilla) and [OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs), which provide block-based interfaces for `NSURLProtocol` HTTP stubbing and mocking.

## How do I generate documentation?

Docsets can be generated from the AFNetworking source code using [appledoc](http://gentlebytes.com/appledoc/). Install `appledoc`, and in the root AFNetworking project directory, enter the following command:

```shell
$ appledoc --project-name="AFNetworking" --project-company="AFNetworking" --company-id="com.afnetworking.afnetworking" AFNetworking/*.h
```

## What's with the "AF" prefix?

The "AF" in AFNetworking stands for "Alamofire", which is the former name of [Gowalla](http://en.wikipedia.org/wiki/Gowalla). Alamofire Inc. was named after the [Alamo Fire](http://aggie-horticulture.tamu.edu/wildseed/alamofire.html), a hybrid of [the Bluebonnet](http://en.wikipedia.org/wiki/Bluebonnet_(plant))--Texas' state flower.

Using AF is also a nod to the "NS" prefix used in Apple's Foundation framework, which harkens back to its [NeXTSTEP](http://en.wikipedia.org/wiki/NeXTSTEP) roots.