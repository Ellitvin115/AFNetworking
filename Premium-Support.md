Premium support for AFNetworking is available to companies and individuals on an hourly basis starting at $200/hour. Initial consultations are provided free-of-charge.

AFNetworking's core contributors are available to help you with your iOS or Mac OS X project, including: 

- Migrating an existing networking infrastructure to AFNetworking
- Writing custom networking code to interface with a web service 
- Troubleshooting issues with AFNetworking
- Optimizing networking and application performance
- Checking for AFNetworking best practices

**[Click here to get in touch and find out more](https://docs.google.com/spreadsheet/viewform?formkey=dHpJNFdYRjVaZjFPNlM4ay1mYUtJVlE6MQ)**