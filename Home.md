This wiki contains additional development information and links to projects and sites using AFNetworking. For all code-related documentation, please refer to the bundled documentation in repository or download.

- Read the ["Getting Started" guide](https://github.com/AFNetworking/AFNetworking/wiki/Getting-Started-with-AFNetworking), 
 
- Look through the [FAQ](https://github.com/AFNetworking/AFNetworking/wiki/AFNetworking-FAQ) for answers to the most commonly-asked questions about AFNetworking.