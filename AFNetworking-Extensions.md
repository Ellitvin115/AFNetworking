Beyond the AFNetworking core lies a constellation of libraries shared by the community to solve more domain-specific problems. Here is a list of some of those:

## Official

- [AFIncrementalStore](https://github.com/AFNetworking/AFIncrementalStore) - Core Data Persistence with AFNetworking
- [AFOAuth1Client](https://github.com/AFNetworking/AFOAuth1Client) - Client for [OAuth 1.0a Authentication](http://oauth.net/core/1.0a/)
- [AFOAuth2Client](https://github.com/AFNetworking/AFOAuth2Client) - Client for [OAuth 2 Authentication](http://oauth.net/2/)
- [AFAmazonS3Client](https://github.com/AFNetworking/AFAmazonS3Client) - Client for the [Amazon S3 API](http://aws.amazon.com/s3/)
- [AFKissXMLRequestOperation](https://github.com/AFNetworking/AFKissXMLRequestOperation) - Extension for [KissXML](https://github.com/robbiehanson/KissXML)
- [AFUrbanAirshipClient](https://github.com/AFNetworking/AFUrbanAirshipClient) - Client for Registering and Unregistering Devices with Urban Airship
- [AFJSONRPCClient](https://github.com/AFNetworking/AFJSONRPCClient) - Client for interacting with JSON-RPC APIs
- [AFCollectionJSONRequestOperation](https://github.com/AFNetworking/AFCollectionJSONRequestOperation) - Extension for [Collection+JSON Hypermedia](http://www.amundsen.com/media-types/collection/)
- [AFHTTPRequestOperationLogger](https://github.com/AFNetworking/AFHTTPRequestOperationLogger) - Extension for HTTP Request Logging
- [Xcode Project Templates](https://github.com/AFNetworking/Xcode-Project-Templates) - New Project Template for AFNetworking, AFIncrementalStore, and CocoaPods. 

## Third-Party

- [AFCalendarRequestOperation](https://github.com/jerolimov/AFCalendarRequestOperation) - Extension for downloading and parsing iCalendar files
- [AFCSVRequestOperation](https://github.com/acerbetti/AFCSVRequestOperation) - Extension for CSV
- [AFDownloadRequestOperation](https://github.com/steipete/AFDownloadRequestOperation) - A progressive download operation class that supports pausing / resuming
- [AFGDataXMLRequestOperation](https://github.com/graetzer/AFGDataXMLRequestOperation) - Extension for GDataXML
- [AFHARchiver](https://github.com/mutualmobile/AFHARchiver) - Extension to generate HTTP Archive (HAR) files of network requests
- [AFHTTPClientLogger](https://github.com/jparise/AFHTTPClientLogger) - Extension for `AFHTTPRequestOperationLogger` to scope operations to `AFHTTPClient` instances 
- [AFNetwork-JSON-RPC-Client](https://github.com/wiistriker/AFNetwork-JSON-RPC-Client) - Client for [JSON-RPC](http://json-rpc.org/)
- [AFProgressiveImageDownload](https://github.com/subdigital/AFProgressiveImageDownload) - Extension for UIImageView for progressively downloading different image resolutions.
- [AFNetworking-ProxyQueue](https://github.com/mystcolor/AFNetworking-ProxyQueue) - Extension for multiple operation queues
- [AFURLConnectionByteSpeedMeasure](https://github.com/OliverLetterer/AFURLConnectionByteSpeedMeasure) - Extension that measures connection speed and estimates completion time.
- [AFXAuthClient](https://github.com/romaonthego/AFXAuthClient) - Client for XAuth Authentication
- [BlockRSSParser](https://github.com/tiboll/BlockRSSParser) - RSS Parser
- [DZWebDAVClient](https://github.com/zwaldowski/DZWebDAVClient) - Client for WebDAV 
- [JJAFAcceleratedDownloadRequestOperation](https://github.com/jnjosh/JJAFAcceleratedDownloadRequestOperation) - Extension for accelerated and resumable downloads
- [KGTouchXMLRequestOperation](https://github.com/kgutteridge/KGTouchXMLRequestOperation) - Extension for [TouchXML](https://github.com/TouchCode/TouchXML)
- [KHGravatar](https://github.com/kcharwood/KHGravatar) - `UIImageView` Category for Fetching Gravatar Images by Email Address

## Frameworks Built on AFNetworking
- [NimbusKit](http://nimbuskit.info) - Framework that provides well-documented, modular components that solve a number of common iOS software requirements.
- [RestKit](https://github.com/RestKit/RestKit) - Framework for consuming and modeling RESTful web resources on iOS and OS X (uses AFNetworking as of version 0.20.0)