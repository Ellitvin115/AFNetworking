github:status
UPDATED 2016-05-07T14:04:49Z
API
If you would like to check GitHub's status from your own applications, we provide a basic JSON API.

All API methods support JSONP, making the information easy to consume in browser applications:

<script>
function apiStatus(data) {
  console.log(data.status);
}
</script>
<script src="https://status.github.com/api/status.json?callback=apiStatus"></script>
List Available Methods

GET /api.json
Returns all methods in the API. Using the URLs returned from this API root, instead of hardcoding the current values, can prevent your client from breaking on URL changes.
{
  "status_url": "https://status.github.com/api/status.json",
  "messages_url": "https://status.github.com/api/messages.json",
  "last_message_url": "https://status.github.com/api/last-message.json"
}
List Current System Status

GET /api/status.json
Returns the current system status--one of good (green), minor (yellow), or major (red)--and timestamp.
{
  "status": "good",
  "last_updated": "2012-12-07T18:11:55Z"
}
List Previous Statuses

GET /api/last-message.json
Returns the last human communication, status, and timestamp.
{
  "status": "good",
  "body": "Battlestation fully operational",
  "created_on": "2012-12-07T18:11:55Z"
}
GET /api/messages.json
Returns the most recent human communications with status and timestamp.
[
  {
    "status": "good",
    "body": "Battlestation fully operational",
    "created_on": "2012-12-07T18:11:55Z"
  },
  {
    "status": "minor",
    "body": "Almost done reticulating splines",
    "created_on": "2012-12-05T12:08:33Z"
  }
]
The GitHub Blog
 
Support
 
Contact
 
API
© 2016 GitHub Inc. All rights reserved.

GitHub.com