Apple announced the transition to IPv6-only network services in iOS 9. Starting June 1, 2016 all apps submitted to the App Store must support IPv6-only networking. 
Is AFNetworking supporting it?
Thanks